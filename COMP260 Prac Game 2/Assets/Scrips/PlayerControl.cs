﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PlayerControl : MonoBehaviour {

	public Vector2 move;
	public float maxSpeed = 5.0f;

	private Rigidbody rigidbody;


	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;
	}



	// Update is called once per frame
	void Update () {
		

		//transform.Translate (move);
	}


	void FixedUpdate(){



		Vector2 direction;
		direction.x = Input.GetAxis("Horizontal");
		direction.y = Input.GetAxis("Vertical");

		Vector2 velocity = direction * maxSpeed;

		rigidbody.velocity = velocity;

		//transform.Translate (velocity * Time.deltaTime);

		//var speed = 5.0f;
		//var maxSpeedx = Input.GetAxis ("Horizontal");
		//var maxSpeedy = Input.GetAxis ("Vertical");
	}
}
