using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {

	private Rigidbody rigidbody;
	public float speed = 20f;
	public float force = 10f;

	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
	


	}
	private Vector3 GetMousePosition(){
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		Plane plane = new Plane (Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast (ray, out distance);
		return ray.GetPoint (distance);


	}
	void OnDrawGizmos(){
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition ();
		Gizmos.DrawLine (Camera.main.transform.position, pos);

	}

	void FixedUpdate(){
		Vector3 pos = GetMousePosition ();
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * speed;

		//rigidbody.AddForce (dir.normalized * force);

		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if(move > distToTarget){
			vel = vel * distToTarget / move;
		}
		rigidbody.velocity = vel;

		//dir = dir.normalized;
		//rigidbody.velocity = dir * speed;

		//rigidbody.MovePosition (pos);
	}



}
