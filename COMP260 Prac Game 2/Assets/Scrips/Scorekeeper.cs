﻿using UnityEngine;
using System.Collections;

public class Scorekeeper : MonoBehaviour {
	public int scorePerGoal = 1;
	private int[] score = new int[2];
	// Use this for initialization
	void Start () {
		Goal[] goals = FindObjectsOfType<Goal> ();
		for (int i = 0; i < goals.Length; i++) {
			goals [i].scoreGoalEvent += OnScoreGoal;

		}
	}

	public void OnScoreGoal(int player){
		score [player] += scorePerGoal;
		Debug.Log ("Player " + player + "; " + score [player]);
	}

	// Update is called once per frame
	void Update () {
	
	}
}
